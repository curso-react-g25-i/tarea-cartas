import React, { Component } from 'react';
import ReactCardFlip from 'react-card-flip';
import { stilos } from '../stilos/index.js';
class Carta extends Component {
  render() {
    const { info, seleccionarCarta, estaSiendoComparada } = this.props;
    const { fueAdivinada } = info;

    const mostrarCarta = estaSiendoComparada || fueAdivinada;
    console.log(mostrarCarta);
    return (
      <div className='card my-2' style={{ width: '125px', height: '125px' }}>
        <ReactCardFlip isFlipped={mostrarCarta} flipDirection='horizontal'>
          <div
            className='card-body p-0'
            style={stilos.backCarta}
            onClick={seleccionarCarta}
          >
            <p className='card-text'>
              <small>React</small>
            </p>
          </div>
          <div className='card-body p-0' style={stilos.frontCarta}>
            <p className='card-text'>
              <i className={`fa ${info.icono}`}></i>
            </p>
          </div>
        </ReactCardFlip>
      </div>
    );
  }
}

export default Carta;
