import React, { Component } from 'react';
import Header from './components/Header';
import Tablero from './components/Tablero';

import baraja from './utils/baraja';

class App extends Component {
  state = {
    baraja: baraja(),
    parejaSeleccionada: [],
    estamosComparando: false,
    numeroDeIntentos: 0,
  };

  seleccionarCarta = () => (carta) => {
    // console.log('Mir la carta seleccionada');
    // console.log(carta);
    /**
     * Si estamos comparando no volteamos la carta O
     * Si la carta fue adivinada no volteamos la carta O
     * Si la carta seleccionada, esta en el arreglo de comparación tampoco volteamos la carta
     */
    if (
      this.estamosComparando ||
      carta.fueAdivinada ||
      this.state.parejaSeleccionada.indexOf(carta) > -1
    ) {
      return;
    }

    const parejaSeleccionada = [...this.state.parejaSeleccionada, carta];
    // console.log('parejaSeleccionada');
    // console.log(parejaSeleccionada);
    // this.setState({ ...this.state, parejaSeleccionada: parejaSeleccionada });

    this.setState((state) => {
      return { ...state, parejaSeleccionada: parejaSeleccionada };
    });

    if (parejaSeleccionada.length === 2) {
      this.compararPareja(parejaSeleccionada);
    }
  };

  compararPareja = (arregloComparador) => {
    this.setState((state) => {
      return { ...state, estamosComparando: true };
    });
    // this.setState({ ...this.state, estamosComparando: true });

    setTimeout(() => {
      const [primeraCarta, segundaCarta] = arregloComparador;
      let baraja = this.state.baraja;
      if (primeraCarta.icono === segundaCarta.icono) {
        baraja = baraja.map((carta) => {
          if (carta.icono !== primeraCarta.icono) {
            return carta;
          }
          return { ...carta, fueAdivinada: true };
        });
      }
      // console.log(baraja);
      this.verificamosSiHayGanador(baraja);
      this.setState({
        baraja: baraja,
        parejaSeleccionada: [],
        estamosComparando: false,
        numeroDeIntentos: this.state.numeroDeIntentos + 1,
      });
    }, 1000);
  };

  verificamosSiHayGanador = (barajaActualizada) => {
    if (barajaActualizada.filter((carta) => !carta.fueAdivinada).length === 0) {
      alert('Ganaste el Juego');
    }
  };

  render() {
    // console.log(this.state.baraja);

    return (
      <div className='container'>
        <Header />
        <Tablero
          baraja={this.state.baraja}
          seleccionarCarta={this.seleccionarCarta()}
          parejaSeleccionada={this.state.parejaSeleccionada}
        />
      </div>
    );
  }
}

export default App;
