export const stilos = {
  backCarta: {
    background: '#ffb300',
    height: '125px',
    borderRadius: '5px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    color: '#000',
    fontSize: '2em',
  },
  frontCarta: {
    background: '#03dcf4',
    height: '125px',
    borderRadius: '5px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    color: '#000',
    fontSize: '5em',
  }
};
