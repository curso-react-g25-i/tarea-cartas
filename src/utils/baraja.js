import shuffle from 'lodash.shuffle';
import FontAwesomeClases from './fontAwasomeClases';

const NUMERO_CARTAS = 20;

export default () => {
  const fontAwesomeClases = FontAwesomeClases();
  let cartas = [];

  while (cartas.length < NUMERO_CARTAS) {
    const index = Math.floor(Math.random() * fontAwesomeClases.length);
    const carta = {
      icono: fontAwesomeClases.splice(index, 1)[0],
      fueAdivinada: false,
    };
    cartas.push(carta);
    cartas.push({ ...carta });
  }

  return shuffle(cartas);
};
